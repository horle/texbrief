# `texbrief`

Ein Skript zum Generieren deutscher DIN-Briefe mit LaTeX.

## Installation

Dieses Programm verwendet `zenity` und das LaTeX-Paket `rechnung`. Bitte installieren Sie diese zunächst über Ihren Paketmanager.

Führen Sie folgende Befehle in dem geklonten Verzeichnis aus (unter der Annahme, dass `~/bin` in Ihrer PATH Variablen liegt):
<code>
$ mkdir -p ~/.config/texbrief
$ cp texbrief.conf *.tex ~/.config/texbrief/
$ cp texbrief ~/bin/
$ chmod +x ~/bin/texbrief
</code>

Editieren Sie folgende Zeilen in der Datei `texbrief.conf`:
<code>
DEFAULT_DIR= >Zielverzeichnis für Ihre Briefe<
TEX_EDITOR= >Ihr Standardeditor für LaTeX<
</code>

## Benutzung
Führen Sie `texbrief` aus und folgen Sie den Anweisungen. Am Ende öffnet sich Ihr Editor zum Bearbeiten des generierten Briefs.
